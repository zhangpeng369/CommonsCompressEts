/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { DumpArchiveEntry, TYPE, DumpArchiveUtil, File, InputStream, OutputStream,
  ArchiveInputStream, ArchiveStreamFactory, ArchiveEntry,
  IOUtils, System } from '@ohos/commons-compress'
import { DumpData } from './DumpDataTest'
import featureAbility from '@ohos.ability.featureAbility';
import fileio from '@ohos.fileio';

@Entry
@Component
export struct DumpTest {
  TAG: string = 'dumpTest----'

  build() {
    Flex({ direction: FlexDirection.Column, alignItems: ItemAlign.Center }) {
      Text('DumpArchiveEntry相关功能')
        .fontSize(20)
        .margin({ top: 16 })
      Text('生成bla.dump文件')
        .fontSize(16)
        .margin({ top: 32 })
        .padding(8)
        .border({ width: 2, color: '#535353', radius: 6 })
        .onClick(() => {
          this.generateTextFile()
        })
      Text('开始调试')
        .fontSize(16)
        .margin({ top: 32 })
        .padding(8)
        .border({ width: 2, color: '#535353', radius: 6 })
        .onClick(() => {
          this.jsDumpTest()
        })
    }
    .width('100%')
    .height('100%')
  }

  generateTextFile(): void {
    var context = featureAbility.getContext();
    context.getFilesDir()
      .then((data) => {
        let srcPath = data + '/dump'
        try {
          fileio.mkdirSync(srcPath);
        } catch (err) {

        }
        const writer = fileio.openSync(srcPath + '/bla.dump', 0o102, 0o666);
        fileio.writeSync(writer, DumpData.buffer);
        fileio.closeSync(writer);
        AlertDialog.show({ title: '生成成功',
          message: '请查看手机路径' + data + '/dump/bla.dump',
          confirm: { value: 'OK', action: () => {

          } }
        })
      })
      .catch((error) => {
        console.error('File to obtain the file directory. Cause: ' + error.message);
      })
  }

  DumpArchiveEntryTest(): void {
    let ent: DumpArchiveEntry = new DumpArchiveEntry("foo", "bar", -1, TYPE.DIRECTORY);
    let a = ent.getSimpleName()
    let b = ent.getOriginalName()
    let c = ent.getName()
    let d = ent.getOffset()
    let e = ent.getMode()
    console.log('a = ' + a + ' b = ' + b + ' c = ' + c)
  }

  DumpArchiveUtilTest(): void {
    let a = DumpArchiveUtil.convert64(new Int8Array([0x80, 0x67, 0x45, 0x23, 1, 0xEF, 0xCD, 0xAB]), 0);
    let b = DumpArchiveUtil.convert32(new Int8Array([1, 0xEF, 0xCD, 0xAB]), 0);
    let c = DumpArchiveUtil.convert16(new Int8Array([0xCD, 0xAB]), 0)
  }

  jsDumpTest(): void {
    var context = featureAbility.getContext();
    context.getFilesDir()
      .then((data) => {

        let timer1 = System.currentTimeMillis().toString()
        console.info(this.TAG + "start::" + timer1)

        this.testDumpUnarchiveAll(data, 'dump/bla.dump')

        let timer2: string = System.currentTimeMillis().toString()
        console.info(this.TAG + "end::" + timer2)

        AlertDialog.show({ title: '解压缩成功',
          message: '请查看手机路径' + data,
          confirm: { value: 'OK', action: () => {

          } }
        })
      }).catch((error) => {
      console.error(this.TAG + 'jsDumpTest File to obtain the file directory. Cause: ' + error.message);
    })
  }

  testDumpUnarchiveAll(data: string, archive: string): void {
    let file1: File = new File(data, archive);
    //    fileio.mkdirSync(data+'/lost+found')
    let input1: InputStream = new InputStream();
    input1.setFilePath(file1.getPath());
    let input2: ArchiveInputStream = null;

    input2 = ArchiveStreamFactory.DEFAULT.createArchiveInputStream("dump", input1, null);

    let entry: ArchiveEntry = input2.getNextEntry();
    while (entry != null) {
      let out: OutputStream = new OutputStream();
      let name: string = entry.getName().toString();
      let archiveEntry: File = new File(data, name);
      archiveEntry.getParentFile().getPath();

      if (entry.isDirectory()) {
        let splitName: string = name.substring(0, name.lastIndexOf('/'));
        try {
          fileio.mkdirSync(data + '/' + splitName);
        } catch (e) {
          console.log(e);
        }
        entry = input2.getNextEntry();
        continue;
      }
      let output: File = new File(data, name);
      out.setFilePath(output.getPath());
      IOUtils.copy(input2, out);
      out.close();
      out = null;
      entry = input2.getNextEntry();
    }
    if (input2 != null) {
      input2.close();
    }
    input1.close();
  }
}
