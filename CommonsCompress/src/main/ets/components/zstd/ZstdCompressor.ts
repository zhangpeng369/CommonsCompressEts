/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import Unsafe from './Unsafe';
import Constants from './Constants'
import Long from '../util/long/index'
import ZstdFrameCompressor from './ZstdFrameCompressor'
import { CompressionParameters } from './CompressionParameters'
import Compressor from './Compressor'

export default class ZstdCompressor implements Compressor {
    public maxCompressedLength(uncompressedSize: number): number
    {
        let result: number = uncompressedSize + (uncompressedSize >>> 8);
        if (uncompressedSize < Constants.MAX_BLOCK_SIZE) {
            result += (Constants.MAX_BLOCK_SIZE - uncompressedSize) >>> 11;
        }
        return result;
    }

    public getZstdFrameCompress(input: Int8Array, inputOffset: number, inputLength: number, output: Int8Array, outputOffset: number, maxOutputLength: number): number
    {
        let inputAddress: Long = Long.fromNumber(Unsafe.ARRAY_BYTE_BASE_OFFSET + inputOffset);
        let outputAddress: Long = Long.fromNumber(Unsafe.ARRAY_BYTE_BASE_OFFSET + outputOffset);

        return ZstdFrameCompressor.compress(input, inputAddress, inputAddress.add(inputLength), output, outputAddress, outputAddress.add(maxOutputLength), CompressionParameters.DEFAULT_COMPRESSION_LEVEL);
    }
}
