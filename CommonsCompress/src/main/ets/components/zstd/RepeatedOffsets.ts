/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
export default class RepeatedOffsets {
    private offset0: number= 1;
    private offset1: number = 4;
    private tempOffset0: number;
    private tempOffset1: number;

    public getOffset0(): number
    {
        return this.offset0;
    }

    public getOffset1(): number
    {
        return this.offset1;
    }

    public saveOffset0(offset: number): void
    {
        this.tempOffset0 = offset;
    }

    public saveOffset1(offset: number): void
    {
        this.tempOffset1 = offset;
    }

    public commit(): void
    {
        this.offset0 = this.tempOffset0;
        this.offset1 = this.tempOffset1;
    }
}
