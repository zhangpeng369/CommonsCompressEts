## 1.0.2
1.由gradle工程整改为hvigor工程。

## 1.0.1
1.适配ark引擎。

## 1.0.0
1.支持bzip2、gzip、lzma、xz、Snappy、LZ4、cpio、deflate、Zstandard 和 ar、tar、zip、7z等格式的压缩/解压功能。

2.支持Brotli、dump、z等格式解压功能。